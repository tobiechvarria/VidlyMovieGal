const mongoose = require('mongoose');
const express = require("express");
const genres = require('./routes/genres');
const customers = require('./routes/customers');

const app = express();

// Mongoose Config
mongoose.connect('mongodb://localhost/vidly')
    .then(() => console.log("connected to vidly database"))
    .catch(err => console.log("Error: ", err));

app.use(express.json());
app.use("/api/genres", genres);
app.use("/api/customers", customers)

// TAKE A LISTEN
const PORT = process.env.PORT || 3000;
app.listen(PORT, () => {
    console.log(`Listening on port ${PORT}...`);
    console.log(app.get('env'));
});