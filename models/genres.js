const mongoose = require('mongoose');
const joi = require("joi");

// GENRE SCHEMA
const genreSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true,
        minlength: 5,
        maxlength: 50
    }
});

const Genre = mongoose.model('Genre', genreSchema)

// HELPER FUNCTIONS
function validateGenre(genre) {
    const schema = {
        name: joi.string().min(3).required()
    }

    return joi.validate(genre, schema);
}

exports.genreSchema = genreSchema;
exports.Genre = Genre;
exports.validate = validateGenre;