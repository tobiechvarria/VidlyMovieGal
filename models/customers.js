const mongoose = require('mongoose');
const JOI = require('joi');

// Customer Schema
const Customer = mongoose.model('Customer', new mongoose.Schema({
    isGold: {
        type: Boolean,
        default: false
    },
    name: {
        type: String,
        required: true,
        minlength: 3,
        maxlength: 55
    },
    phone: {
        type: String,
        minlength: 10,
        maxlength: 10,
        required: true
    }
}));


function validateCustomerInfo(customer) {
    const schema = {
        name: JOI.string().min(3).required(),
        phone: JOI.string().min(10).required(),
        isGold: JOI.boolean()
    }

    return JOI.validate(customer, schema);
}

exports.Customer = Customer;
exports.validate = validateCustomerInfo;