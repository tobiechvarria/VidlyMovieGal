const { Genre, validate } = require('../models/genres');
const mongoose = require('mongoose');
const express = require('express');
const router = express.Router();

// CRUD OPERATIONS

// GET ALL GENRES
router.get("/", async(req, res) => {
    console.log('get')
    const genres = await Genre.find().sort('name');
    res.send(genres);
});

// POST A NEW MOVIE
router.post("/", async(req, res) => {
    // VALIDATE COURSE
    const { error } = validate(req.body);
    if (error) return res.status(400).send(error.details[0].message);

    // ADD COURSE
    let genre = new Genre({ name: req.body.name });
    genre = await genre.save();

    // RETURN COURSE
    res.send(genre);
});

router.put("/:id", async(req, res) => {
    // VALIDATE GENRE FORMAT
    const { error } = validate(req.body);
    if (error) return res.status(400).send(error.details[0].message);

    const genre = await Genre.findByIdAndUpdate(req.params.id, { name: req.body.name }, {
            new: true
        })
        // DOES GENRE EXIST?
    if (!genre) return res.status(404).send(`Genre id ${req.params.id} does not exist`);

    res.send(genre);
});

router.delete("/:id", async(req, res) => {
    // VALIDATE GENRE FORMAT
    const { error } = validate(req.body);
    if (error) return res.status(404).send(error.details[0].message);


    const genre = await Genre.findByIdAndRemove(req.params.id);
    // DOES GENRE EXIST?
    if (!genre) return res.status(404).send(`Genre id ${req.params.id} does not exist`);

    res.send(genre);
});

router.get("/:id", async(req, res) => {
    // VALIDATE GENRE FORMAT
    // const { error } = validate(req.body);
    // if (error) return res.status(404).send(error.details[0].message);

    const genre = await Genre.findById(req.params.id);

    // DOES GENRE EXIST?
    if (!genre) return res.status(404).send(`Genre id ${req.params.id} does not exist`);

    res.send(genre);

});

module.exports = router;