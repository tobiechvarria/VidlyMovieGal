const { Customer, validate } = require('../models/customers');
const mongoose = require('mongoose');
const express = require('express');
const router = express.Router();

// Customer Routes
router.get("/", async(req, res) => {
    const customers = await Customer.find();
    res.send(customers);
});

router.post("/", async(req, res) => {
    // VALIDATE CUSTOMER NAME
    const { error } = validate(req.body);
    if (error) return res.status(400).send(error.details[0].message);

    // ALL GOOD! LETS POST TO MONGODB
    const customer = new Customer({
        name: req.body.name,
        phone: req.body.phone,
        isGold: req.body.isGold
    });
    const result = await customer.save();

    return res.send(result);
});

router.put("/:id", async(req, res) => {
    // VALIDATE CUSTOMER NAME
    const { error } = validate(req.body);
    if (error) return res.status(400).send(error.details[0].message);

    // ALL GOOD! LETS UPDATE!
    const customer = await Customer.findByIdAndUpdate(req.params.id, { name: req.body.name }, {
        new: true // returns new updated document
    });

    if (!customer) return res.status(404).send(`ID ${req.params.id} not found.`);

    res.send(customer);
});

router.get("/:id", async(req, res) => {
    const customer = await Customer.findById(req.params.id);
    if (!customer) return res.status(404).send(`ID ${req.params.id} not found.`);

    return res.send(customer);
});

router.delete("/:id", async(req, res) => {
    const customer = await Customer.findByIdAndRemove(req.params.id);
    if (!customer) return res.status(404).send(`ID ${req.params.id} not found.`);

    return res.send(customer);
});

module.exports = router;